// Initializes the `Schedules` service on path `/schedules`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Schedules } from './schedules.class';
import createModel from '../../models/schedules.model';
import hooks from './schedules.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'schedules': Schedules & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/schedules', new Schedules(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('schedules');

  service.hooks(hooks);
}
