import { Application } from '../declarations';
import users from './users/users.service';
import gpio from './gpio/gpio.service';
import schedules from './schedules/schedules.service';
import snapshots from './snapshots/snapshots.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application): void {
  app.configure(users);
  app.configure(gpio);
  app.configure(schedules);
  app.configure(snapshots);
}
