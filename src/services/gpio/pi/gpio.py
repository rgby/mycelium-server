from __future__ import print_function
import time
from dual_mc33926_rpi import motors, MAX_SPEED
import sys as sys

def print_case(value):
    if value == "0":
      print("motors stop")
      motors.setSpeeds(0, 0)
      motors.disable()
    elif value == "1":
      print("motor1 start")
      motors.enable()
      motors.setSpeeds(450, 0)
    elif value == "2":
      print("motor2 start")
      motors.enable()
      motors.setSpeeds(0, 450)
    else:
        print("Didn't match a case")

if len(sys.argv) > 1:
  print_case(sys.argv[1])
else:
  print("No Argument!")


