// Initializes the `gpio` service on path `/gpio`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Gpio } from './gpio.class';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import FythonService from 'feathers-python';
import * as path from 'path';
// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'gpio': Gpio & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    scriptPath: path.join(__dirname, '/pi/gpio.py'),
    pythonVersion: 'v3'
  };

  app.use('/gpio', new FythonService(options));
}
