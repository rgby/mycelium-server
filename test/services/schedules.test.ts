import assert from 'assert';
import app from '../../src/app';

describe('\'Schedules\' service', () => {
  it('registered the service', () => {
    const service = app.service('schedules');

    assert.ok(service, 'Registered the service');
  });
});
