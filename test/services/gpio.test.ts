import assert from 'assert';
import app from '../../src/app';

describe('\'gpio\' service', () => {
  it('registered the service', () => {
    const service = app.service('gpio');

    assert.ok(service, 'Registered the service');
  });
});
